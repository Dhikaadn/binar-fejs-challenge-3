//Import components, hook, and initialize
import "./App.css";
import React, { useState } from "react";
import TitleSearch from "./components/TitleSearch";
import AddTodo from "./components/AddTodo";
import Todo from "./components/Todo";
import SearchIcon from "@mui/icons-material/Search";
import FilterButton from "./components/FilterButton";
import { TitleList } from "./components/TitleList";
import {
  collection,
  query,
  onSnapshot,
  doc,
  updateDoc,
  deleteDoc,
} from "firebase/firestore";
import { db } from "./firebase";

function App() {
  //Create useState for each function
  const [todos, setTodos] = React.useState([]);
  const [search, setSearch] = useState("");
  const [done, setDone] = useState(false);
  const [all, setAll] = useState(true);
  const [mytodo, setMyTodo] = useState(false);

  //useEffect section
  React.useEffect(() => {
    const q = query(collection(db, "todos"));
    const unsub = onSnapshot(q, (querySnapshot) => {
      let todosArray = [];
      querySnapshot.forEach((doc) => {
        todosArray.push({ ...doc.data(), id: doc.id });
      });
      setTodos(todosArray);
    });
    return () => unsub();
  }, []);

  //Function handleEdit to change the value
  const handleEdit = async (todo, title) => {
    await updateDoc(doc(db, "todos", todo.id), { title: title });
  };

  //Function toggleComplete to change status completed
  const toggleComplete = async (todo) => {
    await updateDoc(doc(db, "todos", todo.id), { completed: !todo.completed });
  };

  //Function handleDelete to delete the value
  const handleDelete = async (id) => {
    await deleteDoc(doc(db, "todos", id));
  };

  //Function handleDeleteAll to delete all value of todos
  const handleDeleteAll = () => {
    setTodos([]);
  };

  //Function onDone to functional button filter Done
  const onDone = () => {
    setDone(!done);
    console.log(`done ${done}`);
  };

  //Function onAll to functional button filter All
  const onAll = () => {
    setAll(all);
    setDone(false);
    setMyTodo(false);
    console.log(`all ${all}`);
  };

  //Function onTodo to functional button filter Todo
  const onTodo = () => {
    setMyTodo(!mytodo);
    setDone(false);
    console.log(`todo ${mytodo}`);
  };

  //Function handleSearch
  const handleSearch = (e) => {
    e.preventDefault();
  };

  //Present the component
  return (
    <div className="App">
      {/* Title of Todo Search */}
      <div>
        <TitleSearch />
      </div>

      {/* Search filter input */}
      <form className="form-search">
        <div className="container-search">
          <div className="bg-teal-500 px-4">
            <SearchIcon className="mt-3" sx={{ color: "white" }} />
          </div>
          <input
            className="border-2 border-slate-300"
            onChange={(e) => setSearch(e.target.value)}
            placeholder="Search Todo"
          />
        </div>
        <div className="btn_container_search mt-5">
          <button type="submit" onClick={handleSearch}>
            Search
          </button>
        </div>
      </form>

      <div>
        {/* Title Todo List */}
        <TitleList />

        {/* AddTodo component */}
        <AddTodo />

        {/* Filter button such as All, Done, and Todo */}
        <FilterButton onAll={onAll} onDone={onDone} onTodo={onTodo} />
      </div>

      <div className="todo_container">
        {todos
          // Filterring to classification between values of All, Done, and Todo
          .filter((todo) => {
            if (done === true) {
              return todo.completed;
            } else if (mytodo === true) {
              return !todo.completed;
            } else if (done === false) {
              return todo;
            } else if (mytodo === false) {
              return todo;
            } else if (all === true) {
              return todo;
            }
          })
          //Filter to functional of search todo
          .filter((todo) => {
            return search.toLowerCase() === ""
              ? todo
              : todo.title.toLowerCase().includes(search);
          })
          .map((todo) => (
            //Send the function to the Todo component
            <Todo
              key={todo.id}
              todo={todo}
              toggleComplete={toggleComplete}
              handleDelete={handleDelete}
              handleEdit={handleEdit}
              onDone={onDone}
            />
          ))}
      </div>

      {/* Component of Delete Button */}
      <div className="DeletedButton">
        <div className="btn_container_delete">
          <button onClick={onTodo}>Delete Done Task</button>
        </div>
        <div className="btn_container_delete">
          <button onClick={handleDeleteAll}>Delete All Task</button>
        </div>
      </div>
    </div>
  );
}
export default App;
