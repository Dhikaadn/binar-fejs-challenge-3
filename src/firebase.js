// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyC0sSnk0J7aC7y1HSHHcpGGvyBOEyCcu8s",
  authDomain: "todo-crud-32f85.firebaseapp.com",
  projectId: "todo-crud-32f85",
  storageBucket: "todo-crud-32f85.appspot.com",
  messagingSenderId: "516230593131",
  appId: "1:516230593131:web:a475caf81db8c63935cf2e",
  measurementId: "G-BLT9CJ42ZG",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

export { db };
