import React from "react";

export default function TitleSearch() {
  return (
    //Title of Todo Search
    <div className="title text-2xl">
      <h1>Todo Search</h1>
    </div>
  );
}
