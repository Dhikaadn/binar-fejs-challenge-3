import React from "react";

//Receive props argument from App.js
function FilterButton({ onAll, onDone, onTodo }) {
  return (

    <div className="FilterButton">
      {/* Button All */}
      <div className="btn_container_filter">
        <button onClick={onAll}>All</button>
      </div>
      {/* Button Done */}
      <div className="btn_container_filter">
        <button onClick={onDone}>Done</button>
      </div>
      {/* Button Todo */}
      <div className="btn_container_filter">
        <button onClick={onTodo}>Todo</button>
      </div>
    </div>
  );
}

export default FilterButton;
