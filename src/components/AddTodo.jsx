import React from "react";
import { db } from "../firebase";
import { collection, addDoc } from "firebase/firestore";

export default function AddTodo() {
  //Create a useState for value of title
  const [title, setTitle] = React.useState("");

  //Function handleSubmit for functional of AddTodo
  const handleSubmit = async (e) => {
    e.preventDefault();
    if (title !== "") {
      await addDoc(collection(db, "todos"), {
        title,
        completed: false,
      });
      setTitle("");
    }
  };
  return (
    //Create a new todo
    <form onSubmit={handleSubmit}>
      <div className="input_container">
        <input
          className="border-2 border-slate-300"
          type="text"
          placeholder="Enter todo..."
          value={title}
          onChange={(e) => setTitle(e.target.value)}
        />
      </div>
      <div className="btn_container">
        <button>Add New Task</button>
      </div>
    </form>
  );
}
