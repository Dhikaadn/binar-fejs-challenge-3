import React from 'react'

export const TitleList = () => {
  return (
    //Title of Todo List
    <div className="title text-2xl">
      <h1>Todo List</h1>
    </div>
  )
}
